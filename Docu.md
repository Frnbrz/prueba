# Modal Component



### Pequeño esquema del refactor:
[Link Refactor esquema](https://lucid.app/lucidchart/invitations/accept/inv_7cca59f4-5308-42b6-92fc-88d3a12cff03)

## Modal Mui 

- Añade de forma nativa on click outside



## Nuevo componente:

>Props: { show, onClose, children, title } 

- show `boolean`: renderiza el modal y evita {showmodal && < Modal />} ahora se pasa como propiedad < Modal show={showmodal} >
- onClose `funcion`
- title `string`
- children  `componente`


## componente
``` 
<ModalMui open={open} onClose={onClose}>
  <div className="modal">
    <div className="modal-content">
      <header className="modal-header">
        <span className="modal-title">{title}</span>
        <span className="modal-close" onClick={onClose}>
          <CloseModalIcon />
        </span>
      </header>
      <main className="modal-main">{children}</main>
    </div>
  </div>
</ModalMui>
``` 

## uso

```
const [show, setShow] = useState(false)

const onClose = () => {
    setShow(false)
}

const onOpen = () => {
    setShow(true)
}

return (
  <div>
    <Modal show={show} 
    onClose={onClose} 
    children={children} 
    title={title}> 
      <ModalForm />
    </Modal>
    <button onClick={onOpen}>
  </div>
)
```


## CreatePortal

>createPortal para poder renderizar fuera del árbol de elementos el modal y permita tener el modal dentro de un componente




# Refactor multiple

## i18n

>Cambio de lugar

Se ha movido de la parte public a src con su carpeta languaje, dentro el archivo i18n y ambas carpetas es y en.

## Redux

>Movidas carpetas y rename

Se ha movido carpetas a las vistas y se han renombrado algunas de ellas

El indice ahora apunta solo a los thunks.js 

Usernavinfo y toast reducer actualizados de reducer a extraReducers con thunks 

## Estilos

>Sass y bootstrap

Sacado bootstrap de header y la dependencia react-bootstrap, los componentes de react-bootstrap no se usaban

Importado de node modules los archivos de bootstrap para incorporarlo a sass 

Modificado los imports y añadido variables de sass 



